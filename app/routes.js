var Post = require("./models/post"),
	path = require("path");

module.exports = function(app) {
	// server routes ===========================================================
    // handle things like api calls
    // authentication routes
    app.get('/api/posts', function(req, res) {
        // use mongoose to get all posts in the database
        Post.find(function(err, posts) {

            // if there is an error retrieving, send the error. 
                            // nothing after res.send(err) will execute
            if (err) {
                console.log(err);
            } else {
            	res.json(posts); // return all posts in JSON format
            }
        });
    });

    // route to handle creating goes here (app.post)
    // route to handle delete goes here (app.delete)

	// frontend routes =========================================================
    // route to handle all angular requests
    //     catchall
	app.get("*", function(req, res) {
		//								  1 lvl up
		res.sendFile(path.join(__dirname + "/../" + "public/index.html"));
	});
};