var express 	   = require("express"),
	path 		   = require("path"),
	mongoose	   = require("mongoose"),
	bodyParser 	   = require("body-parser"),
	methodOverride = require("method-override"),
	db 			   = require("./config/db");


// CONNECT TO DB
mongoose.connect(db.url);

var app = express();
//var routes = require("./app/routes")(app); // Configure the routes.
var port = process.env.PORT || "8080";

app.use(bodyParser.urlencoded({extended: true}));
// app.use(bodyParser.json());
// app.use(bodyParser.json({ type: 'application/vnd.api+json' }));
// Allows use of PUT, PATCH, DELETE POST methods
app.use(methodOverride("X-HTTP-Method-Override"));
// Serve images, CSS files, and JavaScript files in a directory named 'public'.
app.use(express.static(__dirname + '/public'));

// ==============ROUTES===========================================
require("./app/routes")(app);


// ==============START============================================
// HEY LISTEN!
app.listen(port, function() {
	console.log("Magic happens on port : " + port);
});

// EXPOSE
module.exports = app;