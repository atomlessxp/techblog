// Register a new service
angular.module('PostService', []).factory('Post', ['$http', function($http) {
	return {
		// Get all posts
		get : function() {
			return $http.get('/api/posts');
		},
		// Create a new post
		create : function(postData) {
			return $http.post('/api/posts', postData);
		},
		// Delete a post
		delete : function(id) {
			return $http.delete('api/posts' + id);
		}
	}
}]);