// DEFINE OUR ANGULAR ROUTES HERE
angular.module('appRoutes', []).config(['$routeProvider', '$locationProvider', function(routeProvider, locationProvider) {
	// Home page
	$routeProvider.when('/', {
		templateUrl: 'views/home.html',
		controller: 'MainController'
	})
	// Posts page
	.when('/posts', {
		templateUrl: 'views/posts.html',
		controller: 'PostController'
	});

	// Config deep linking paths
	$locationProvider.html5Mode(true);
}]);